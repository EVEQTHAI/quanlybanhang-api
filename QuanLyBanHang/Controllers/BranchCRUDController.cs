﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QuanLyBanHang.Model;
using QuanLyBanHang.Repository;

namespace QuanLyBanHang.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BranchCRUDController : ControllerBase
    {
        private readonly IBranchRepository _branchRepo;

        public BranchCRUDController(IBranchRepository repo) 
        {
            _branchRepo =  repo;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllBranch()
        {
            try
            {
                return Ok(await _branchRepo.GetAllBranchAsync());
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetBranchById(int id)
        {
            var branch = await _branchRepo.GetBranchAsync(id);
            return branch == null ? NotFound() : Ok(branch);
        }

        [HttpPost]
        public async Task<IActionResult> AddNewBranch(BranchModel model)
        {
            try
            {
                var newBranch = await _branchRepo.AddNewBranchAsync(model);
                var branch = await _branchRepo.GetBranchAsync(newBranch);
                return branch == null ? NotFound() : Ok(branch);
            }
            catch
            {
                return NotFound();
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateBranch(int id, BranchModel model)
        {
            try
            {
                await _branchRepo.UpdateBranchAsync(id, model);
                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBranch(int id)
        {
            try
            {
                await _branchRepo.DeleteBranchAsync(id);
                return Ok();
            }
            catch
            { 
                return BadRequest();
            }
        }
    }
}
