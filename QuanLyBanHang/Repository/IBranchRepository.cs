﻿using QuanLyBanHang.Data;
using QuanLyBanHang.Model;

namespace QuanLyBanHang.Repository
{
    public interface IBranchRepository
    {
        public Task<List<BranchModel>> GetAllBranchAsync();
        public Task<BranchModel> GetBranchAsync(int id);
        public Task<int> AddNewBranchAsync(BranchModel branchModel);
        public Task UpdateBranchAsync(int id, BranchModel branchModel);
        public Task DeleteBranchAsync(int id);
    }
}
