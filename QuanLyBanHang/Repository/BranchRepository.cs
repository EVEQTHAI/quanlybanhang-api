﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using QuanLyBanHang.Data;
using QuanLyBanHang.Model;

namespace QuanLyBanHang.Repository
{
    public class BranchRepository : IBranchRepository
    {
        private readonly BanHangContext _context;
        private readonly IMapper _mapper;

        public BranchRepository(BanHangContext context,IMapper mapper) 
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<int> AddNewBranchAsync(BranchModel branchModel)
        {
            var newBranch = _mapper.Map<Branch>(branchModel);
            _context.Branchs!.Add(newBranch);
            await _context.SaveChangesAsync();
            return newBranch.BranchId;
        }

        public async Task DeleteBranchAsync(int id)
        {
            var deleteBranch = _context.Branchs!.SingleOrDefault(p=>p.BranchId == id);
            if(deleteBranch != null)
            {
                _context.Branchs!.Remove(deleteBranch);
                await _context.SaveChangesAsync();
            }
        }

        public async Task<List<BranchModel>> GetAllBranchAsync()
        {
            var books = await _context.Branchs!.ToListAsync();
            return _mapper.Map<List<BranchModel>>(books);
        }

        public async Task<BranchModel> GetBranchAsync(int id)
        {
            var book = await _context.Branchs!.FindAsync(id);
            return _mapper.Map<BranchModel>(book);
        }

        public async Task UpdateBranchAsync(int id, BranchModel branchModel)
        {
            if(id == branchModel.BranchId)
            {
                var updateBranch  = _mapper.Map<Branch>(branchModel);
                _context.Update(updateBranch);
                await _context.SaveChangesAsync();
            }
        }
    }
}
