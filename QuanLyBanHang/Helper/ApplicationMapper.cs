﻿using AutoMapper;
using Microsoft.Build.Framework.Profiler;
using QuanLyBanHang.Data;
using QuanLyBanHang.Model;

namespace QuanLyBanHang.Helper
{
    public class ApplicationMapper:Profile
    {
        public ApplicationMapper() 
        {
            CreateMap<Branch,BranchModel>().ReverseMap();
        }
    }
}
