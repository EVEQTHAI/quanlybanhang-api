﻿using Microsoft.EntityFrameworkCore;

namespace QuanLyBanHang.Data
{
    public class BanHangContext:DbContext
    {
        public BanHangContext(DbContextOptions<BanHangContext>opt):base(opt) 
        {

        }
        #region DBset
        public DbSet<Branch>? Branchs { get; set; }
        #endregion
    }
}
 