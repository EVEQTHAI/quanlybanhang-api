﻿using Microsoft.EntityFrameworkCore;
using QuanLyBanHang.Data;
using Microsoft.Extensions.DependencyInjection;
using QuanLyBanHang.Repository;

var builder = WebApplication.CreateBuilder(args);


// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

//1. Khai báo API
builder.Services.AddCors(option => option.AddDefaultPolicy(policy=>policy.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod()));
//7. khai báo add DB context
builder.Services.AddDbContext<BanHangContext>(options => {
    options.UseSqlServer(builder.Configuration.GetConnectionString("BanHang"));
});
//14. Khai báo AutoMapper
builder.Services.AddAutoMapper(typeof(Program));
//16.Khai báo life cycle DI: AddSingleton(), AddTransient(), AddScoped()
builder.Services.AddScoped<IBranchRepository, BranchRepository>();//chỉ cần dùng hoặc gọi Interface để sử dụng 

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
