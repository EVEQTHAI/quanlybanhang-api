﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Data;
using System.Diagnostics;
using System.Net.Http.Headers;
using WebBanHang.Models;

namespace WebBanHang.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        //tạo 1 đương dẫn đến API để lấy dữ liệu
        string baseURL = "https://localhost:7039/api/";

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        //chuyển lại async bất đồng bộ 
        public async Task<IActionResult> Index()
        {
            //calling the web API using dataTable
            //1.tạo 1 biến cục bộ lưu trử dữ liệu từ API dưới dạng table
            DataTable dt = new DataTable();
            //2.tạo đối tượng HttpClient để thực hiện yêu cầu API
            using (var client = new HttpClient())
            {
                //3.truyền địa chị cơ sở API
                client.BaseAddress = new Uri(baseURL);
                //4.đặt tiêu chuẩn cho yêu cầu ở đây là JSON
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //5.gửi yêu cầu get đến địa chỉ ("") và đợi phản hồi
                HttpResponseMessage getData = await client.GetAsync("Branches");
                if (getData.IsSuccessStatusCode)
                {
                    //6.chuyển đối chuổi JSON thành thành 1 dataTable ==> dễ dàng hiển thị trên giao diện người dùng
                    string result = getData.Content.ReadAsStringAsync().Result;
                    dt = JsonConvert.DeserializeObject<DataTable>(result);
                }
                else
                {
                    Console.WriteLine("Error calling web API");
                }

                //6. trả về view và hiện thị giao diện người dùng
                ViewData.Model = dt;

            }


            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}